# -*- coding: utf-8 -*-
require 'rubygems'
require 'bundler/setup'
require 'opencv'
include OpenCV

if ARGV.size != 1
  puts "Usage: ruby #{__FILE__} Image"
  exit
end

image = nil
image_laugh = nil
begin
  image = IplImage.load ARGV[0], 1
  image_laugh = IplImage.load './laugh.png', 1
rescue
  puts 'Could not open or find the image.'
  exit
end

detector = CvHaarClassifierCascade::load './haarcascade_frontalface_alt.xml'
detector.detect_objects(image).each do |region|
  resized_image = image_laugh.resize region
  image.set_roi region
  (resized_image.rows * resized_image.cols).times do |i|
    if resized_image[i][0].to_i > 0 or resized_image[i][1].to_i > 0 or resized_image[i][2].to_i > 0
      image[i] = resized_image[i]
    end
  image.reset_roi
  end
end

window = GUI::Window.new('display.rb')
window.show image
GUI::wait_key
