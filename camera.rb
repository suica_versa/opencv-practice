#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require 'rubygems'
require 'bundler/setup'
require 'opencv'
require 'debugger'
include OpenCV

# モザイク処理関数
def mosaic(img, rect, size)
  Range.new(rect.y, rect.y+rect.height).step(size) do |y|
    Range.new(rect.x, rect.x+rect.width).step(size) do |x|
      w = h = size
      w = (img.width - x) if x+size > img.width
      h = (img.height - y) if y+size > img.height
      topleft = CvPoint.new(x, y)
      bottomright = CvPoint.new(x+w, y+h)
      # 部分領域を抜き出してavgで色の平均値を取得
      avg = img.sub_rect(topleft, CvSize.new(w, h)).avg
      # 領域を平均値の色で塗りつぶす
      img.rectangle!(topleft, bottomright, :color => avg, :thickness => -1)
    end
  end
end

# Window作成
window = GUI::Window.new('face mosaic')
# カメラでのキャプチャ開始
capture = CvCapture.open(0)
face = CvHaarClassifierCascade::load('./haarcascade_frontalface_alt.xml')


while GUI::wait_key(100).nil?
  img = capture.query
  # iSight(MacbookAirのカメラ)があたたまるまで待つ
  if img.instance_of?(OpenCV::IplImage)
    face.detect_objects(img) do |region|
      color = OpenCV::CvColor::Blue
      img.rectangle! region.top_left, region.bottom_right, :color => color
      mosaic(img, region, 20)
    end
    window.show img
  end
end

window.destroy
